public class Contatore {
	
	public int getValore() {
		return valore;
	}

	public void setValore(int valore) {
		this.valore = valore;
	}

	private int valore=0;

	public static void main(String[] args) {
		Contatore c= new Contatore();
		c.setValore(6);
		System.out.println("Valore del contatore: "+c.getValore());

	}

}
